﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Security.Encryption;

namespace  LendFoundry.SyndicationTracker.Persistance
{
    public class SyndicationTrackerRepositoryFactory : ISyndicationTrackerRepositoryFactory
    {
#region Constructor
        public SyndicationTrackerRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
#endregion

#region Private Properties
        private IServiceProvider Provider { get; }
#endregion

#region Public Methods
        public ISyndicationTrackerRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
	        var mongoConfigurationFactory =Provider.GetService<IMongoConfigurationFactory>();					
	        var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());							
	        return new SyndicationTrackerRepository(tenantService, configuration);
        }
#endregion
    }
}