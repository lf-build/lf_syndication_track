﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.SyndicationTracker.Persistance {
    public class SyndicationTrackerRepository : MongoRepository<ISyndicationTracker, SyndicationTracker>,
        ISyndicationTrackerRepository {
            static SyndicationTrackerRepository () {
                BsonClassMap.RegisterClassMap<SyndicationTracker> (map => {
                    map.AutoMap ();
                    map.MapProperty (p => p.Status).SetSerializer (new EnumSerializer<Status> (BsonType.String));
                    var type = typeof (SyndicationTracker);
                    map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass (true);
                });
                BsonClassMap.RegisterClassMap<TimeBucket> (map => {
                    map.AutoMap ();
                    map.MapMember (m => m.Time).SetSerializer (new DateTimeOffsetSerializer (BsonType.Document));

                    var type = typeof (TimeBucket);
                    map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass (false);
                });
            }

            public SyndicationTrackerRepository (ITenantService tenantService, IMongoConfiguration configuration) : base (tenantService, configuration, "syndication-tracker") {
              
            }

            public async Task<ISyndicationTracker> Get (string entityType, string entityId, string callSource, string methodName) {
                return await Query.FirstOrDefaultAsync (syndication => syndication.EntityType == entityType && syndication.EntityId == entityId && syndication.SourceSyndication == callSource && syndication.MethodName == methodName);
            }

             public async Task<List<ISyndicationTracker>> GetAllTrackDetails (string entityType, string entityId) {
                 return await Query.Where(syndication => syndication.EntityType == entityType && syndication.EntityId == entityId).ToListAsync();
            }
            public async Task<ISyndicationTracker> AddOrUpdate (ISyndicationTracker syndicationCall) {
                var SyndicationTracker = await Get (syndicationCall.EntityType, syndicationCall.EntityId, syndicationCall.SourceSyndication, syndicationCall.MethodName);

                if (SyndicationTracker == null) {
                    SyndicationTracker.AttemptCount = 1;
                    Add (syndicationCall);
                    return SyndicationTracker;
                }

                SyndicationTracker.Status = syndicationCall.Status;
                SyndicationTracker.ExceptionDetails = syndicationCall.ExceptionDetails;
                SyndicationTracker.AttemptCount += syndicationCall.AttemptCount;
                SyndicationTracker.ReInitiateCount += syndicationCall.ReInitiateCount;

                Update (SyndicationTracker);

                return SyndicationTracker;
            }

        }
}