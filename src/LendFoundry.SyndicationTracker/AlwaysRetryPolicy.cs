﻿using System;

namespace LendFoundry.SyndicationTracker
{
    public class AlwaysRetryPolicy : IRecoverabilityPolicy
    {
        public bool CanBeRecoveredFrom(Exception ex)
        {
            return true;
        }
    }

}
