﻿using LendFoundry.SyndicationTracker;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration;
//using LendFoundry.Security.Identity.Client;


namespace LendFoundry.SyndicationTracker
{
    public class SyndicationTrackerServiceFactory : ISyndicationTrackerServiceFactory
    {
        public SyndicationTrackerServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public ISyndicationTrackerService Create(ITokenReader reader, ILogger logger, ITokenHandler handler)
        {
            var repositoryFactory = Provider.GetService<ISyndicationTrackerRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);
           
            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);
        
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var configurationService = configurationServiceFactory.Create<SyndicationTrackerConfiguration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

          
            return new  SyndicationTrackerService(repository,eventHub, tenantTime,configuration,reader, handler);
        }
    }
}
