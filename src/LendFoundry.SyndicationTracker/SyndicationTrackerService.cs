﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.SyndicationTracker;
using Microsoft.CSharp.RuntimeBinder;

namespace LendFoundry.SyndicationTracker
{
    public class SyndicationTrackerService : ISyndicationTrackerService
    {
        #region Constructor

        public SyndicationTrackerService
            (
                ISyndicationTrackerRepository syndicationTrackerRepository,
                IEventHubClient eventHubClient,
                ITenantTime tenantTime,
                SyndicationTrackerConfiguration configuration,
                ITokenReader tokenReader,
                ITokenHandler tokenParser

            )
        {
            TenantTime = tenantTime;
            EventHubClient = eventHubClient;
            Configuration = configuration;
            SyndicationTrackerRepository = syndicationTrackerRepository;
            TokenReader = tokenReader;
            TokenParser = tokenParser;

        }

        #endregion

        #region Private Properties
        private ISyndicationTrackerRepository SyndicationTrackerRepository { get; }
        private ITenantTime TenantTime { get; }
        private SyndicationTrackerConfiguration Configuration { get; }
        private IEventHubClient EventHubClient { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }

        //private IEntityStatusService EntityStatusService { get; }

        #endregion

        #region Public Method
        public async Task<ISyndicationTracker> TrackSyndicationCall(string entityType, string entityId, ISyndicationTracker syndicationCall)
        {

            EnsureRequest(syndicationCall);

            var existingData = await SyndicationTrackerRepository.Get(entityType, entityId, syndicationCall.SourceSyndication, syndicationCall.MethodName);

            var sourceConfig = Configuration.Syndications.FirstOrDefault(x => x.Name.ToLower() == syndicationCall.SourceSyndication.ToLower());
            if (sourceConfig == null)
                throw new NotFoundException($"Configuration not found for {syndicationCall.SourceSyndication}");

            var sourceMethodConfig = sourceConfig.Methods.FirstOrDefault(x => x.Name.ToLower() == syndicationCall.MethodName.ToLower());
            if (sourceMethodConfig == null)
                throw new NotFoundException($"Configuration not found for {syndicationCall.MethodName}");

            if (existingData == null)
            {
                syndicationCall.AttemptCount = 1;
                syndicationCall.ReInitiateCount = 0;
                syndicationCall.IsReInitiate = false;
                SyndicationTrackerRepository.Add(syndicationCall);

            }
            else
            {
                existingData.AttemptCount += 1;
                existingData.ReInitiateCount += 1;
                existingData.IsReInitiate = syndicationCall.IsReInitiate;
                existingData.Status = syndicationCall.Status;
                existingData.ExceptionDetails = syndicationCall.ExceptionDetails;
                existingData.SecondaryName = syndicationCall.SecondaryName;
                existingData.SecondaryKey = syndicationCall.SecondaryKey;
                existingData.CreatedOn = syndicationCall.CreatedOn;
                existingData.CreatedBy = syndicationCall.CreatedBy;
                SyndicationTrackerRepository.Update(existingData);
                syndicationCall = existingData;

            }
            return syndicationCall;
        }

        public async Task<List<ISyndicationTracker>> GetAllSyndicationTrackDetail(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Entity type is required.");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Application number is required.");

            var callTrackDetails = await SyndicationTrackerRepository.GetAllTrackDetails(entityType, entityId);

            return callTrackDetails;
        }

        public async Task<bool> AllowReinitiate(string entityType, string entityId, string source, string method)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Entity type is required.");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Application number is required.");

            if (string.IsNullOrWhiteSpace(source))
                throw new ArgumentException("Syndication source name is required.");

            if (string.IsNullOrWhiteSpace(method))
                throw new ArgumentException("Syndication method name is required.");

            var existingData = await SyndicationTrackerRepository.Get(entityType, entityId, source, method);

            var sourceConfig = Configuration.Syndications.FirstOrDefault(x => x.Name.ToLower() == source.ToLower());
            if (sourceConfig == null)
                throw new NotFoundException($"Configuration not found for {source}");

            var sourceMethodConfig = sourceConfig.Methods.FirstOrDefault(x => x.Name.ToLower() == method.ToLower());
            if (sourceMethodConfig == null)
                throw new NotFoundException($"Configuration not found for {method}");

            if (existingData != null && existingData.ReInitiateCount >= sourceMethodConfig.ReInitiate.MaxReAttemptCount)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Private Methods
        private Dictionary<string, string> EntityTypes { get; set; }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            // if (EntityTypes == null)
            //     EntityTypes = LookupService.GetLookupEntries("entityTypes");

            if (!EntityTypes.ContainsKey(entityType))
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }

        private void EnsureRequest(ISyndicationTracker syndicationCall)
        {
            //syndicationCall.EntityType = EnsureEntityType(syndicationCall.EntityType);

            if (string.IsNullOrWhiteSpace(syndicationCall.EntityType))
                throw new ArgumentException("Entity type is required.");

            if (string.IsNullOrWhiteSpace(syndicationCall.EntityId))
                throw new ArgumentException("Application number is required.");

            if (string.IsNullOrWhiteSpace(syndicationCall.SourceSyndication))
                throw new ArgumentException("Syndication source name is required.");

            if (string.IsNullOrWhiteSpace(syndicationCall.MethodName))
                throw new ArgumentException("Syndication method name is required.");

            if (!Enum.IsDefined(typeof(Status), syndicationCall.Status))
                throw new ArgumentNullException($"Syndication call {nameof(syndicationCall.Status)} is mandatory");

            syndicationCall.CreatedOn = new TimeBucket(TenantTime.Now);
            syndicationCall.CreatedBy = EnsureCurrentUser();
        }

        private string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("User is not authorized");
            return username;
        }

        private static string GetResultValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data[0].result;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static string GetResultValueForDynamic(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.result;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        #endregion
    }
}