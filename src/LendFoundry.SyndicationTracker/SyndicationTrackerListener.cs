﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace LendFoundry.SyndicationTracker
{
    public class SyndicationTrackerListener : ISyndicationTrackerListener
    {
        #region Constructor
        public SyndicationTrackerListener
        (
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            ISyndicationTrackerServiceFactory syndicationTrackerServicFactory,
            IConfigurationServiceFactory configurationFactory,
            ISyndicationTrackerRepositoryFactory syndicationTrackerRepositoryFactory)
        {

            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(syndicationTrackerServicFactory), syndicationTrackerServicFactory);
            EnsureParameter(nameof(tenantTimeFactory), tenantTimeFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);    
            EnsureParameter(nameof(syndicationTrackerRepositoryFactory), syndicationTrackerRepositoryFactory);                

            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            SyndicationTrackerServiceFactory = syndicationTrackerServicFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;        
            SyndicationTrackerRepositoryFactory = syndicationTrackerRepositoryFactory;        
        }
        #endregion

        #region Private Properties       
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private ISyndicationTrackerServiceFactory SyndicationTrackerServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; } 

         private ISyndicationTrackerRepositoryFactory SyndicationTrackerRepositoryFactory  { get; }     
        #endregion

        #region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenantId = Environment.GetEnvironmentVariable("TenantId");
                logger.Info($"Processing tenant #{tenantId}");
                var token = TokenHandler.Issue(tenantId, Settings.ServiceName, null, "system", null);
              
                var reader = new StaticTokenReader(token.Value);
                var eventHub = EventHubFactory.Create(reader);
                var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                var configuration = ConfigurationFactory.Create<SyndicationTrackerConfiguration>(Settings.ServiceName, reader).Get();

                var SyndicationTrackerService = SyndicationTrackerServiceFactory.Create(reader,logger,TokenHandler);
                
                if (configuration != null && configuration.events != null)
                {
                    configuration
                   .events
                   .ToList().ForEach(events =>
                   {
                       eventHub.On(events.Name, ProcessEvent(events, logger, SyndicationTrackerService));
                       logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                   });

                    logger.Info("-------------------------------------------");
                }
                else
                {
                    logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                }
                eventHub.StartAsync();

                logger.Info("Syndication Tracker listener started");
            }
            catch (Exception ex)
             {
                logger.Error("Error while listening eventhub to process Syndication Tracker listener", ex);
                logger.Info("\n Syndication Tracker listener  is working yet and waiting new event\n");
                Start();
            }
        }

        #endregion

        #region Private Methods

        #region Process Event
        private Action<LendFoundry.EventHub.EventInfo> ProcessEvent(
           EventConfiguration eventConfiguration,
           ILogger logger,
           ISyndicationTrackerService syndicationTrackerService
           )
        {
            return async @event =>
            {
                string responseData=string.Empty;
                string source = string.Empty;
                string sourceMethod = string.Empty;
                object data = null;

                try
                {
                    try
                    {
                        responseData = eventConfiguration.Response.FormatWith(@event); //posted data
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
                    }
                    catch { }

                        var entityId = eventConfiguration.EntityId.FormatWith(@event);
                        var entityType = eventConfiguration.EntityType.FormatWith(@event);
                        var Name =  eventConfiguration.Name.FormatWith(@event);
                        source = eventConfiguration.SourceName.FormatWith(@event); 
                        sourceMethod = eventConfiguration.SourceMethodName.FormatWith(@event); 
                        var status = eventConfiguration.Status.FormatWith(@event); 
                        var secondaryKey = eventConfiguration.SecondaryKey.FormatWith(@event); 
                        var secondaryName = eventConfiguration.SecondaryName.FormatWith(@event); 
       
                    Enum.TryParse(status, out Status syndicationStatus);
                    ISyndicationTracker request = new SyndicationTracker
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        SourceSyndication = source,
                        MethodName = sourceMethod,
                        Status = syndicationStatus,
                        ExceptionDetails = responseData,
                        SecondaryKey = secondaryKey,
                        SecondaryName = secondaryName
                    };
                  
                   var detail = await syndicationTrackerService.TrackSyndicationCall(entityType,entityId,request);
                    
                    logger.Info($"Entity Id: #{entityId}");
                    logger.Info($"Response Data : #{data}");
                    logger.Info($"Syndication Tracker event {Name} completed for {entityId} ");

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name}", ex);
                }
            };
        }
        #endregion

        #region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
        #endregion

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response, exception);
            }
        }
        #endregion
        #endregion

    }
}
