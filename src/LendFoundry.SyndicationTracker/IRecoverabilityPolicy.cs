﻿using System;

namespace LendFoundry.SyndicationTracker
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}
