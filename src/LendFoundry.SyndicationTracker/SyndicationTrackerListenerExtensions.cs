﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

#endif


namespace LendFoundry.SyndicationTracker
{
    public static class SyndicationTrackerListenerExtensions
    {
        public static void UseSyndicationTrackerListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<ISyndicationTrackerListener>().Start();
        }
    }
}
