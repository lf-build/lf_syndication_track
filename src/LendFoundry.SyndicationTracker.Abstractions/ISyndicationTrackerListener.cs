﻿namespace LendFoundry.SyndicationTracker
{
    public interface ISyndicationTrackerListener
    {
        void Start();
    }
}
