﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.SyndicationTracker
{
    public interface ISyndicationTrackerRepository : IRepository<ISyndicationTracker>
    {
       Task<ISyndicationTracker> Get(string entityType, string entityId, string callSource, string methodName);
       Task<ISyndicationTracker> AddOrUpdate(ISyndicationTracker syndicationCall);

       Task<List<ISyndicationTracker>> GetAllTrackDetails(string entityType, string entityId);

    }
}