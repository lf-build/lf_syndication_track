﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.SyndicationTracker
{
    public interface ISyndicationTrackerServiceFactory
    {
        ISyndicationTrackerService Create(ITokenReader reader, ILogger logger, ITokenHandler tokenHandler);
    }
}
