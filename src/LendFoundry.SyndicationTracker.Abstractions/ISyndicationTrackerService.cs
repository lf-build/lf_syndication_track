﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.SyndicationTracker
{
    public interface ISyndicationTrackerService
    {
       //Task<ISyndicationTracker> Get(string entityType, string entityId, string callSource, string methodName);
      Task<ISyndicationTracker> TrackSyndicationCall(string entityType, string entityId, ISyndicationTracker syndicationCall);

      Task<List<ISyndicationTracker>> GetAllSyndicationTrackDetail(string entityType, string entityId);
    
    Task<bool> AllowReinitiate(string entityType, string entityId, string source, string method);
    }
}