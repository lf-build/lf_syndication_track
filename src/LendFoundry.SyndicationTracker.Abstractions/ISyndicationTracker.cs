﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;


namespace LendFoundry.SyndicationTracker
{
    public interface ISyndicationTracker : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string SecondaryKey { get; set; }

        string SecondaryName { get; set; }
        string SourceSyndication { get; set; }
        string MethodName { get; set; }
        Status Status { get; set; }
        string ExceptionDetails { get; set; }
        int AttemptCount { get; set; }
        bool IsReInitiate { get; set; }
        int ReInitiateCount { get; set; }
        TimeBucket CreatedOn { get; set; }
        string CreatedBy { get; set; }

    }
}
