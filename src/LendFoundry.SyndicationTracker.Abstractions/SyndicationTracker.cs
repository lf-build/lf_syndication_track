﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace  LendFoundry.SyndicationTracker
{
    public class SyndicationTracker : Aggregate,ISyndicationTracker
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string SecondaryName { get; set; }
        public string SecondaryKey { get; set; }
        public string SourceSyndication { get; set; }
        public string MethodName { get; set; }
        public Status Status { get; set; }

        public string ExceptionDetails { get; set; }
        public int AttemptCount { get; set; }

        
        public bool IsReInitiate { get; set; }
        public int ReInitiateCount { get; set; }

        public TimeBucket CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}