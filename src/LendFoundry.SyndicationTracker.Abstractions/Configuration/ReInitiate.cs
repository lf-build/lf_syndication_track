﻿using System;
using System.Collections.Generic;

namespace LendFoundry.SyndicationTracker
{
    public class ReInitiate : IReInitiate
    {
        public bool IsActive { get; set; }
        public bool IsReinitiateViaActionMenu { get; set; }
        public int MaxReAttemptCount { get; set; }
        public   bool IsRenitiate  { get; set; }
        public List<string> ApplicationStatus { get; set; }
    }
}
