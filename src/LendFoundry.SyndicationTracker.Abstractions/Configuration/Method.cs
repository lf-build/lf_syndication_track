﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.SyndicationTracker
{
    public class Method : IMethod
    {
        public Method()
        {
            ReInitiate = new ReInitiate();
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool IsDisplayOnBackOffice { get; set; }
        public bool AllowMultipleRecord { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IReInitiate, ReInitiate>))]
        public IReInitiate ReInitiate { get; set; }
    }
}
