﻿
namespace LendFoundry.SyndicationTracker
{
    public class EventConfiguration
    {
        public string EntityId { get; set; }
        public string Response { get; set; }
        public string Name { get; set; }
         public string EntityType { get; set; }
        public string SourceName { get; set; }
        public string MethodToExecute { get; set; }
        public string SourceMethodName { get; set; }
        public string ResponseObject { get; set; }
        public string Status { get; set; }
        public string SecondaryKey { get; set; }
         public string SecondaryName { get; set; }
    }
}