﻿using System.Collections.Generic;


namespace LendFoundry.SyndicationTracker
{
    public interface IReInitiate
    {
         bool IsActive { get; set; }
         bool IsReinitiateViaActionMenu { get; set; }
         int MaxReAttemptCount { get; set; }
         bool IsRenitiate  { get; set; }
         List<string> ApplicationStatus { get; set; }
    }
}