﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.SyndicationTracker
{
    public class Syndication : ISyndication
    {
        public Syndication()
        {
            Methods = new List<IMethod>();
        }
        public string Name { get; set; }
        public int MaxAttemptCount { get; set; }
        public bool IsActive { get; set; }
        
        [JsonConverter(typeof(InterfaceListConverter<IMethod, Method>))]
        public List<IMethod> Methods { get; set; }

      
    }
}
