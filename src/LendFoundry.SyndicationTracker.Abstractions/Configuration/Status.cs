﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.SyndicationTracker
{
    public enum Status
    {
        NotInitiated = 0,
        Initiated = 1,
        Failure = 2,
        Completed = 3,
        Exception = 4
    }
}
