﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.SyndicationTracker
{
    public interface IMethod
    {
        string Name { get; set; }
        string DisplayName { get; set; }
        bool IsDisplayOnBackOffice { get; set; }
        bool AllowMultipleRecord { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IReInitiate, ReInitiate>))]
        IReInitiate ReInitiate { get; set; }
    }
}