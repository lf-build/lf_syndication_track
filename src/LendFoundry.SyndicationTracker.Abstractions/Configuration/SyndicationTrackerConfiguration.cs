﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.SyndicationTracker
{
    public class SyndicationTrackerConfiguration : IDependencyConfiguration 
    {
        [JsonConverter(typeof(InterfaceListConverter<ISyndication, Syndication>))]
        public List<ISyndication> Syndications { get; set; }
        public List<EventConfiguration> events { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
       
    }
}
