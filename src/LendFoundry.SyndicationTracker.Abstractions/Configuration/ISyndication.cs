﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.SyndicationTracker
{
    public interface ISyndication
    {
         string Name { get; set; }
         int MaxAttemptCount { get; set; }
         bool IsActive { get; set; }
        
        [JsonConverter(typeof(InterfaceListConverter<IMethod, Method>))]
         List<IMethod> Methods { get; set; }
     
    }
}