﻿namespace LendFoundry.SyndicationTracker
{
    public enum SourceType
    {
        Syndication,
        Form,
        Automatic,
        Review,
        Polling
    }
}