﻿namespace LendFoundry.SyndicationTracker
{
    public interface ISyndicationTrackerHistory : ISyndicationTracker
    {
         string SyndicationTrackId { get; set; }

    }
}
