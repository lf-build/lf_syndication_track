﻿using System;

using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;


namespace  LendFoundry.SyndicationTracker
{
    public interface ISyndicationTrackerRepositoryFactory
    {
#region Public Methods
        ISyndicationTrackerRepository Create(ITokenReader reader);
      
#endregion
    }
}