﻿
using System;

namespace LendFoundry.SyndicationTracker
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "syndication-tracker";


    }
}