﻿using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using RestSharp.Extensions;
using System.Collections.Generic;
using System.Net;
using System;
using System.Linq;
using System.Net.Http.Headers;
using LendFoundry.SyndicationTracker;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.SyndicationTracker.Api
{
    /// <summary>
    /// SyndicationTrackController class
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    [Route("/")]
    public class SyndicationTrackController : ExtendedController
    {
        /// <summary>
        /// SyndicationTrackController constructor
        /// </summary>
        /// <param name="syndicationTrackService">The verification engine service.</param>
        public SyndicationTrackController(ISyndicationTrackerService syndicationTrackService)
        {
            SyndicationTrackService = syndicationTrackService;
        }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        private ISyndicationTrackerService SyndicationTrackService { get; }

        /// <summary>
        /// Verifies the facts.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost("{entityType}/{entityId}")]
        // [ProducesResponseType(typeof(ISyndicationTracker), 200)]
        // [ProducesResponseType(typeof(ErrorResult), 404)]
        // [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> TrackSyndicationCall(string entityType, string entityId, [FromBody]SyndicationTracker request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok((await SyndicationTrackService.TrackSyndicationCall(entityType, entityId, request)));
            });
        }

        /// <summary>
        /// Verifies the facts.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}")]
        // [ProducesResponseType(typeof(ISyndicationTracker), 200)]
        // [ProducesResponseType(typeof(ErrorResult), 404)]
        // [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllTrackDetails(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok((await SyndicationTrackService.GetAllSyndicationTrackDetail(entityType, entityId)));
            });
        }

        /// <summary>
        /// AllowReinitiation
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="source"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{entityId}/{source}/{method}")]
        public async Task<IActionResult> AllowReinitiation(string entityType, string entityId,string source, string method)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok((await SyndicationTrackService.AllowReinitiate(entityType, entityId,source,method)));
            });
        }
    }
}
