﻿
using LendFoundry.SyndicationTracker;
using LendFoundry.SyndicationTracker.Persistance;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Security.Encryption;
//using CreditExchange.StatusManagement.Client;
using System;
using System.Runtime;
using LendFoundry.Configuration;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.SyndicationTracker.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {

            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Syndication Track"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.SyndicationTracker.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif

            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<SyndicationTrackerConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<SyndicationTrackerConfiguration>(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
           // services.AddStatusManagementService();
           // services.AddDataAttributes();
            services.AddLookupService();
            services.AddMongoConfiguration(Settings.ServiceName);
            //services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<SyndicationTrackerConfiguration>(p => p.GetService<IConfigurationService<SyndicationTrackerConfiguration>>().Get());
            // internals
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<SyndicationTrackerConfiguration>>().Get());
            services.AddTransient<ISyndicationTrackerService, SyndicationTrackerService>();
            services.AddTransient<ISyndicationTrackerRepository, SyndicationTrackerRepository>();
            services.AddTransient<ISyndicationTrackerListener, SyndicationTrackerListener>();
            services.AddTransient<ISyndicationTrackerServiceFactory, SyndicationTrackerServiceFactory>();
            services.AddTransient<ISyndicationTrackerRepositoryFactory, SyndicationTrackerRepositoryFactory>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
          
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Syncation Call Track");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseSyndicationTrackerListener();
            app.UseMvc();
        }
    }
}
