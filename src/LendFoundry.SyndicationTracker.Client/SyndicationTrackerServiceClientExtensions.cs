﻿using System;
using Microsoft.Extensions.DependencyInjection;
using LendFoundry.Security.Tokens;

namespace LendFoundry.SyndicationTracker.Client
{
   
    public static class SyndicationTrackerClientExtensions
    {
        public static IServiceCollection AddSyndicationTrackerService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddSingleton<ISyndicationTrackerServiceClientFactory>(p => new SyndicationTrackerServiceClientFactory(p, endpoint, port));
            services.AddSingleton(p => p.GetService<ISyndicationTrackerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSyndicationTrackerService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ISyndicationTrackerServiceClientFactory>(p => new SyndicationTrackerServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<ISyndicationTrackerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddSyndicationTrackerService(this IServiceCollection services)
        {
            services.AddSingleton<ISyndicationTrackerServiceClientFactory>(p => new SyndicationTrackerServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<ISyndicationTrackerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
