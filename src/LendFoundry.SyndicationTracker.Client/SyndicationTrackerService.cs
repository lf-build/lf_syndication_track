﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System;
using LendFoundry.SyndicationTracker;


namespace LendFoundry.SyndicationTracker.Client
{
    public class SyndicationTrackerService : ISyndicationTrackerService
    {
        #region Constructors
        public SyndicationTrackerService(IServiceClient client)
        {
            Client = client;
        }
        #endregion

        #region Private Properties
        private IServiceClient Client { get; }
        #endregion

        #region Public Methods
        public async Task<bool> AllowReinitiate(string entityType, string entityId, string source, string method)
        {
            var request = new RestRequest("{entityType}/{entityId}/{source}/{method}", RestSharp.Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("source", source);
            request.AddUrlSegment("method", method);
            return await Client.ExecuteAsync<bool>(request);
        }

       public Task<ISyndicationTracker> TrackSyndicationCall(string entityType, string entityId, ISyndicationTracker syndicationCall)
        {
            throw new NotImplementedException();
        }

          public  Task<List<ISyndicationTracker>> GetAllSyndicationTrackDetail(string entityType, string entityId)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}