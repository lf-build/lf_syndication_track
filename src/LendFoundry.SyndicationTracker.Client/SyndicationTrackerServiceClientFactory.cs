﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.SyndicationTracker.Client
{
    public class SyndicationTrackerServiceClientFactory : ISyndicationTrackerServiceClientFactory
    {
        #region Constructor
        public SyndicationTrackerServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public SyndicationTrackerServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private int CachingExpirationInSeconds { get; }
        private Uri Uri { get; }
        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        #endregion

        #region Public Methods
        public ISyndicationTrackerService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("syndication-tracker");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new SyndicationTrackerService(client);

        }
        #endregion

    }
}
