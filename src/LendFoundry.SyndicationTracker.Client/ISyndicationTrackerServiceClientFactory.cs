﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.SyndicationTracker.Client
{
    public interface ISyndicationTrackerServiceClientFactory
    {
        ISyndicationTrackerService Create(ITokenReader reader);
    }
}
